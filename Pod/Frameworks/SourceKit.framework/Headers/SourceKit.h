//
//  SourceKit.h
//  SourceKit
//
//  Created by Isambard Poulson on 05/11/2015.
//  Copyright © 2015 Isambard Poulson. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SourceKit.
FOUNDATION_EXPORT double SourceKitVersionNumber;

//! Project version string for SourceKit.
FOUNDATION_EXPORT const unsigned char SourceKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SourceKit/PublicHeader.h>
#import <SourceKit/HISourceKit.h>