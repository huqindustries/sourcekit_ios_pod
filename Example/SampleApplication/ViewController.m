//
//  ViewController.m
//  SampleApplication
//
//  Created by Isambard Poulson on 05/11/2015.
//  Copyright © 2015 Isambard Poulson. All rights reserved.
//

#import "ViewController.h"
#import "HISourceKit.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController ()

@property (strong,nonatomic) CLLocationManager *locationManager;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager requestAlwaysAuthorization];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
