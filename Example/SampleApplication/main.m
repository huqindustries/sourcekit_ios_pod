//
//  main.m
//  SampleApplication
//
//  Created by Isambard Poulson on 05/11/2015.
//  Copyright © 2015 Isambard Poulson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
