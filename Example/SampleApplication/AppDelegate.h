//
//  AppDelegate.h
//  SampleApplication
//
//  Created by Isambard Poulson on 05/11/2015.
//  Copyright © 2015 Isambard Poulson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

