//
//  HISourceKit.m
//  HIRegionMonitor
//
//  Created by Isambard Poulson on 03/11/2015.
//  Copyright © 2015 Huq Industries. All rights reserved.
//

#import "HISourceKit.h"
#import <CoreLocation/CoreLocation.h>
#import "Huqability.h"
#import <mach/mach.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import <UIKit/UIKit.h>

#ifdef DEBUG
#import <CocoaLumberjack/CocoaLumberjack.h>
#endif

@interface HISourceKit()<CLLocationManagerDelegate>

@property (strong,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) NSMutableArray *regionStatuses;
@property (strong,nonatomic) CLLocation *previousLocation;
@property (atomic) BOOL regionEventRecentlyTriggered;
@property (nonatomic) UIBackgroundTaskIdentifier taskIdentifier;
@property (strong,nonatomic) NSTimer *backgroundTimer;

@property (strong,nonatomic) NSMutableArray *regionAnnotations;

@property (strong, nonatomic) NSString *apiKey;
@property (strong, nonatomic) NSString *previousBSSID;
@property (strong, nonatomic) NSMutableArray *eventsArray;

@property (nonatomic) BOOL isRecording;
@property (atomic) BOOL debugMode;

@end

// Geo settings
static CGFloat const metresInOneGeoDegree = 111111;
static CGFloat const regionRadius = 50.0;

// Regions
static NSString* const monitoredRegions = @"monitoredRegions";
static NSString* const huqRegionId = @"HIR";
static NSString* const centralRegionLabel = @"C";
static NSString* const northRegionLabel = @"N";
static NSString* const northEastRegionLabel = @"NE";
static NSString* const southEastRegionLabel = @"SE";
static NSString* const southRegionLabel = @"S";
static NSString* const southWestRegionLabel = @"SW";
static NSString* const northWestRegionLabel = @"NW";

// API settings
static NSString *kHuqEndpoint = @"https://api.huq.io/analyse";
static NSString *kHuqLibVersion = @"1.1";
static NSString *kHuqVersion = @"HuqVersion";
static NSString *kHuqUID = @"HuqUID";
static NSString *kHuqSrcOS = @"HuqSrcOS";
static NSString *kHuqTimeDate = @"HuqTimeDate";
static NSString *kHuqLat = @"HuqLat";
static NSString *kHuqLng = @"HuqLng";
static NSString *kHuqAcc = @"HuqAcc";
static NSString *kHuqSSID = @"HuqSSID";
static NSString *kHuqBSSID = @"HuqBSSID";
static NSString *kHuqEvents = @"HuqEvents";
static NSString *kHuqKey = @"HuqKey";
static NSString *kHuqBundleId = @"HuqBundleId";
static NSString *kHuqForegroundMode = @"HuqForegroundMode";
static NSString *kHuqBackgroundMode = @"HuqBackgroundMode";

#ifdef DEBUG
static const int ddLogLevel = DDLogLevelDebug;
#endif

@implementation HISourceKit


#pragma mark - Singleton

+ (id) sharedKit {
    
    static HISourceKit *sharedKit = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedKit = [[HISourceKit alloc] init];
    });
    return sharedKit;
}


#pragma mark - Initialisation

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        
        [self.locationManager startMonitoringSignificantLocationChanges];
        
        self.regionEventRecentlyTriggered = NO;
        
        [self configureReachability];
        [self configureBackgroundTask];
        
        self.eventsArray = [[NSMutableArray alloc] init];
        
#ifdef DEBUG
        report_memory();
#endif
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - API

- (void) recordWithAPIKey:(NSString *)apiKey inDebugMode:(BOOL)debugMode
{
    self.debugMode = debugMode;
    
    // test OS version; SourceKit has been built for iOS 8+
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0 && self.debugMode) {
        NSLog(@"Huq_DEBUG: iOS 8.0+ required.");
        return;
    }
    
    // check that the API key submitted is of the correct form
    if ([apiKey rangeOfString:@".{8}[-].{4}[-].{4}[-].{4}[-].{12}" options:NSRegularExpressionSearch].location != NSNotFound)
    {
        self.apiKey = apiKey;
        self.isRecording = YES;
    }else {
        
        NSLog(@"Huq_DEBUG: malformed API Key.");
        return;
    }
    
    self.previousBSSID = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterForegroundHandler:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)stopRecording
{
    self.isRecording = NO;
}

- (void) logCustomEventWithTags:(NSArray *)tags
{
    [self.eventsArray addObjectsFromArray:tags];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if(status == kCLAuthorizationStatusAuthorizedAlways)
    {
        [self configureRegions];
    }
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    [self regionEventHandler];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    [self regionEventHandler];
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
#ifdef DEBUG
    DDLogDebug(@"HIRM : monitoringDidFailForRegion :: %@",region.identifier);
#endif
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
}

#pragma mark - Region handling

- (void)regionEventHandler
{
    @synchronized(self)
    {
#ifdef DEBUG
        DDLogDebug(@"HIRM : regionEventHandler : 0");
#endif
        
        if(self.regionEventRecentlyTriggered)
            return;
#ifdef DEBUG
        DDLogDebug(@"HIRM : regionEventHandler : 1");
#endif
        
        self.regionEventRecentlyTriggered = YES;
        
        [self clearRegions];
        
        [self configureRegions];
        
        [self configureBackgroundTask];
        
        [self checkForSubmission];
        
        [self performSelector:@selector(resetRegionEventRecentlyTriggered)
                   withObject:nil
                   afterDelay:1];
    }
}

- (void)configureRegions
{
    CLLocation* currentLocation = self.locationManager.location;
    
    if(currentLocation)
    {
        self.regionAnnotations = [[NSMutableArray alloc] init];
        
        CLRegion* centralRegion = [[CLCircularRegion alloc] initWithCenter:currentLocation.coordinate
                                                                    radius:regionRadius
                                                                identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,centralRegionLabel]];
        [self.locationManager stopMonitoringForRegion:centralRegion];
        [self.locationManager startMonitoringForRegion:centralRegion];
        
        CLLocationDistance entryRegionsOffsetMetres = regionRadius * 1.5;
        CLLocationDegrees entryRegionsOffsetDegrees = entryRegionsOffsetMetres / metresInOneGeoDegree;
        
        CLLocationCoordinate2D northCoordinate = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude + entryRegionsOffsetDegrees, currentLocation.coordinate.longitude);
        CLRegion* northRegion = [[CLCircularRegion alloc] initWithCenter:northCoordinate
                                                                  radius:regionRadius
                                                              identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,northRegionLabel]];
        [self.locationManager stopMonitoringForRegion:northRegion];
        [self.locationManager startMonitoringForRegion:northRegion];
        
        CLLocationCoordinate2D northEastCoordinate = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude + entryRegionsOffsetDegrees * sin(M_PI / 6), currentLocation.coordinate.longitude + entryRegionsOffsetDegrees * cos(M_PI / 6));
        CLRegion* northEastRegion = [[CLCircularRegion alloc] initWithCenter:northEastCoordinate
                                                                      radius:regionRadius
                                                                  identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,northEastRegionLabel]];
        [self.locationManager stopMonitoringForRegion:northEastRegion];
        [self.locationManager startMonitoringForRegion:northEastRegion];
        
        CLLocationCoordinate2D southEastCoordinate = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude - entryRegionsOffsetDegrees * sin(M_PI / 6), currentLocation.coordinate.longitude + entryRegionsOffsetDegrees * cos(M_PI / 6));
        CLRegion* southEastRegion = [[CLCircularRegion alloc] initWithCenter:southEastCoordinate
                                                                      radius:regionRadius
                                                                  identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,southEastRegionLabel]];
        [self.locationManager stopMonitoringForRegion:southEastRegion];
        [self.locationManager startMonitoringForRegion:southEastRegion];
        
        CLLocationCoordinate2D southCoordinate = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude - entryRegionsOffsetDegrees, currentLocation.coordinate.longitude);
        CLRegion* southRegion = [[CLCircularRegion alloc] initWithCenter:southCoordinate
                                                                  radius:regionRadius
                                                              identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,southRegionLabel]];
        [self.locationManager stopMonitoringForRegion:southRegion];
        [self.locationManager startMonitoringForRegion:southRegion];
        
        CLLocationCoordinate2D southWestCoordinate = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude - entryRegionsOffsetDegrees * sin(M_PI / 6), currentLocation.coordinate.longitude - entryRegionsOffsetDegrees * cos(M_PI / 6));
        CLRegion* southWestRegion = [[CLCircularRegion alloc] initWithCenter:southWestCoordinate
                                                                      radius:regionRadius
                                                                  identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,southWestRegionLabel]];
        [self.locationManager stopMonitoringForRegion:southWestRegion];
        [self.locationManager startMonitoringForRegion:southWestRegion];
        
        CLLocationCoordinate2D northWestCoordinate = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude + entryRegionsOffsetDegrees * sin(M_PI / 6), currentLocation.coordinate.longitude - entryRegionsOffsetDegrees * cos(M_PI / 6));
        CLRegion* northWestRegion = [[CLCircularRegion alloc] initWithCenter:northWestCoordinate
                                                                      radius:regionRadius
                                                                  identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,northWestRegionLabel]];
        [self.locationManager stopMonitoringForRegion:northWestRegion];
        [self.locationManager startMonitoringForRegion:northWestRegion];
        
        self.previousLocation = currentLocation;
    }
}

- (void)clearRegions
{
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(0.0, 0.0);
    
    CLRegion* centralRegion = [[CLCircularRegion alloc] initWithCenter:coordinate
                                                                radius:10.0
                                                            identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,centralRegionLabel]];
    [self.locationManager stopMonitoringForRegion:centralRegion];
    
    CLRegion* northRegion = [[CLCircularRegion alloc] initWithCenter:coordinate
                                                              radius:10.0
                                                          identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,northRegionLabel]];
    [self.locationManager stopMonitoringForRegion:northRegion];
    
    CLRegion* northEastRegion = [[CLCircularRegion alloc] initWithCenter:coordinate
                                                                  radius:10.0
                                                              identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,northEastRegionLabel]];
    [self.locationManager stopMonitoringForRegion:northEastRegion];
    
    CLRegion* southEastRegion = [[CLCircularRegion alloc] initWithCenter:coordinate
                                                                  radius:10.0
                                                              identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,southEastRegionLabel]];
    [self.locationManager stopMonitoringForRegion:southEastRegion];
    
    CLRegion* southRegion = [[CLCircularRegion alloc] initWithCenter:coordinate
                                                              radius:10.0
                                                          identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,southRegionLabel]];
    [self.locationManager stopMonitoringForRegion:southRegion];
    
    CLRegion* southWestRegion = [[CLCircularRegion alloc] initWithCenter:coordinate
                                                                  radius:10.0
                                                              identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,southWestRegionLabel]];
    [self.locationManager stopMonitoringForRegion:southWestRegion];
    
    CLRegion* northWestRegion = [[CLCircularRegion alloc] initWithCenter:coordinate
                                                                  radius:10.0
                                                              identifier:[NSString stringWithFormat:@"%@ : %@",huqRegionId,northWestRegionLabel]];
    [self.locationManager stopMonitoringForRegion:northWestRegion];
}

- (void)resetRegionEventRecentlyTriggered
{
    self.regionEventRecentlyTriggered = NO;
}

#pragma mark - Significant change

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [self checkForSubmission];
    [self configureRegions];
}

#pragma mark - Timer

- (void)backgroundTimerHandler
{
#ifdef DEBUG
    DDLogDebug(@"HIRM : background time remaining : %lu",(long)[[UIApplication sharedApplication] backgroundTimeRemaining]);
#endif
    
    if([[UIApplication sharedApplication] backgroundTimeRemaining] < 10)
    {
#ifdef DEBUG
        DDLogDebug(@"HIRM : background time below threshold");
#endif
        [self cleanupTimer];
        [self cleanupBackgroundTask];
    }
}

- (void)cleanupTimer
{
#ifdef DEBUG
    report_memory();
    
    DDLogDebug(@"HIRM : cleanup timer");
#endif
    
    [self.backgroundTimer invalidate];
    self.backgroundTimer = nil;
}

#pragma mark - Background Task

- (void)configureBackgroundTask
{
#ifdef DEBUG
    report_memory();
    
    DDLogDebug(@"HIRM : start background task : 0");
#endif
    
    if(self.backgroundTimer != nil)
        return;
    
#ifdef DEBUG
    DDLogDebug(@"HIRM : start background task : 1");
#endif
    
    [self cleanupBackgroundTask];
    self.taskIdentifier = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
#ifdef DEBUG
        DDLogDebug(@"HIRM : BACKGROUND TASK EXPIRED!!");
#endif
        [self cleanupTimer];
        [self cleanupBackgroundTask];
    }];
    
    self.backgroundTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                            target:self
                                                          selector:@selector(backgroundTimerHandler)
                                                          userInfo:nil
                                                           repeats:YES];
}

- (void)cleanupBackgroundTask
{
#ifdef DEBUG
    report_memory();
    
    DDLogDebug(@"HIRM : cleanup background task");
#endif
    
    [[UIApplication sharedApplication] endBackgroundTask:self.taskIdentifier];
    self.taskIdentifier = UIBackgroundTaskInvalid;
}


#pragma mark - Reachability

- (void)configureReachability
{
    Huqability* reach = [Huqability reachabilityWithHostname:@"api.huq.io"];
    
    reach.reachableOnWWAN = NO;
    
    // Set the blocks
    
    __weak HISourceKit* weakSelf = self;
    
    reach.reachableBlock = ^(Huqability*reach)
    {
        // keep in mind this is called on a background thread
        // and if you are updating the UI it needs to happen
        // on the main thread, like this:
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf checkForSubmission];
#ifdef DEBUG
            DDLogDebug(@"HIRM : REACHABLE!");
#endif
        });
    };
    
    reach.unreachableBlock = ^(Huqability*reach)
    {
        [weakSelf checkForSubmission];
#ifdef DEBUG
        DDLogDebug(@"HIRM : UNREACHABLE!");
#endif
    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
}


#pragma mark - Huq stuff


- (NSArray *)getPreparedEventsArray
{
    [self.eventsArray addObject:([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive) ? kHuqForegroundMode : kHuqBackgroundMode];
    
    return [[NSSet setWithArray:self.eventsArray] allObjects];
    
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"

/*
 *  Method to fetch currently-connected SSID string
 */

- (NSString *)ssid {
    
    // init retVal with null
    
    NSString *ssidString = @"";
    
    // get the list of available WiFi networks
    
    CFArrayRef interfaces = CNCopySupportedInterfaces();
    
    // if there are any networks in the list
    
    if (interfaces) {
        
        // get the first (and only)
        
        CFDictionaryRef networkDetails = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(interfaces, 0));
        CFRelease(interfaces);
        
        // retrieve the details from the found network
        
        if (networkDetails) {
            
            // retrieve the SSID value from the details
            
            ssidString = (NSString *)CFDictionaryGetValue(networkDetails, kCNNetworkInfoKeySSID);
            CFRelease(networkDetails);
            
        }
    }
    return ssidString;
}

/*
 *  Method to fetch currently-connected BSSID string
 */

- (NSString *)bssid {
    
    // init retVal with null
    
    NSString *bssidString = @"";
    
    // get the list of available WiFi networks
    
    CFArrayRef interfaces = CNCopySupportedInterfaces();
    
    // if there are any networks in the list
    
    if (interfaces) {
        
        // get the first (and only)
        
        CFDictionaryRef networkDetails = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(interfaces, 0));
        CFRelease(interfaces);
        
        // retrieve the details from the found network
        
        if (networkDetails) {
            
            // retrieve the SSID value from the details
            
            bssidString = (NSString *)CFDictionaryGetValue(networkDetails, kCNNetworkInfoKeyBSSID);
            CFRelease(networkDetails);
            
        }
    }
    
    return bssidString;
}

#pragma clang diagnostic pop

- (BOOL)shouldUpdateWiFi {
    
    // bool value indicating that the network values are different
    
    BOOL shouldUpdate = (![[self bssid] isEqualToString:self.previousBSSID]);
    
    // update if it's different and has a value.
    
    return (shouldUpdate && [self bssid].length);
}

- (void)didEnterForegroundHandler:(NSNotification*)notification
{
    [self checkForSubmission];
    [self configureRegions];
}

- (void)checkForSubmission
{
    if (self.debugMode)
    {
        NSLog(@"Huq_DEBUG: shouldUpdate = %i", [self shouldUpdateWiFi]);
    }
    
    if([[self bssid] isEqualToString:@""])
    {
        self.previousBSSID = @"";
    }
    
    if ([self shouldUpdateWiFi] && self.isRecording)
    {
        [self submitResult];
    }
}

- (void)submitResult
{
    // if in debug mode, log whether and why updates should
    // or should not be made at server
    
    CLLocation* mostRecentLocation = self.locationManager.location;
    
    NSMutableDictionary *compiledData = [NSMutableDictionary dictionary];
    
    [compiledData setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:kHuqUID];
    
    NSString* systemName = [[UIDevice currentDevice] systemName];
    NSString* systemVersion = [[UIDevice currentDevice] systemVersion];
    NSString* deviceOS = [NSString stringWithFormat:@"%@_%@", systemName, systemVersion];
    [compiledData setObject:deviceOS forKey:kHuqSrcOS];
    [compiledData setObject:[NSString stringWithFormat:@"%@",[NSDate date]] forKey:kHuqTimeDate];
    [compiledData setObject:@(mostRecentLocation.coordinate.latitude) forKey:kHuqLat];
    [compiledData setObject:@(mostRecentLocation.coordinate.longitude) forKey:kHuqLng];
    [compiledData setObject:@(mostRecentLocation.horizontalAccuracy) forKey:kHuqAcc];
    [compiledData setObject:[self ssid] forKey:kHuqSSID];
    [compiledData setObject:[self bssid] forKey:kHuqBSSID];
    [compiledData setObject:self.apiKey forKey:kHuqKey];
    [compiledData setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:kHuqBundleId];
    [compiledData setObject:[self getPreparedEventsArray] forKey:kHuqEvents];
    
    // if in debug mode, log contents of dictionary
    
    if (self.debugMode)
        NSLog(@"Huq_DEBUG:\n%@\n", compiledData);
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:compiledData options:0 error:&error];
    
    // if there is an error, and if in debug mode, log to console
    
    if (error) {
        if (self.debugMode)
            NSLog(@"Huq_DEBUG:\nError creating POST data for web-service request\n%@\n", [error description]);
        return;
    }
    
    // build the webservice endpoint
    
    NSString *serviceEndpoint = [NSString stringWithFormat:@"%@/%@/", kHuqEndpoint, kHuqLibVersion];
    
    // create the webservice request
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serviceEndpoint]];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:jsonData];
    
    // POST the webservice request.
    // if the request fails, it will fail silently (fine).
    
    [[[NSURLSession sharedSession] dataTaskWithRequest:request
                                     completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                         [self handleResponse:(NSHTTPURLResponse*)response];
                                     }] resume];
    
    self.previousBSSID = [self bssid];
}

/*
 *  Method to reset custom events list and communicate
 *  web-service status via console log in development
 */

- (void)handleResponse:(NSHTTPURLResponse*) httpResponse {
    
    // reset custom events overlay
    
    self.eventsArray = [[NSMutableArray alloc] init];
    
    // request went okay; log to console
    
    if (self.debugMode)
    {
        if ([httpResponse statusCode] == 200)
        {
            NSLog(@"Huq_DEBUG: Request Complete");
        }else
        {
            NSLog(@"Huq_DEBUG: Request Failed");
        }
    }
}


#pragma mark - Memory

#ifdef DEBUG
void report_memory(void) {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kerr = task_info(mach_task_self(),
                                   TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        DDLogDebug(@"HIRM : Memory in use (in bytes): %u", info.resident_size);
    } else {
        DDLogDebug(@"HIRM : Error with task_info(): %s", mach_error_string(kerr));
    }
}
#endif

@end
