# SourceKit

[![Version](https://img.shields.io/cocoapods/v/SourceKit.svg?style=flat)](http://cocoapods.org/pods/SourceKit)
[![License](https://img.shields.io/cocoapods/l/SourceKit.svg?style=flat)](http://cocoapods.org/pods/SourceKit)
[![Platform](https://img.shields.io/cocoapods/p/SourceKit.svg?style=flat)](http://cocoapods.org/pods/SourceKit)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SourceKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SourceKit"
```

## Author

Isambard Poulson,  isambard@huq.io

## License

SourceKit is available under an All Rights Reserved license. See the LICENSE file for more info.
